/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import java.io.FileNotFoundException;
import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author luis_
 */
public class Functions {
    public void ReadConfig (String path,JTextArea text) throws FileNotFoundException{
        String[] dados={};
        String comando = "";
            Scanner in = new Scanner(new FileReader(path));
     while (in.hasNextLine()) {
        String line = in.nextLine();
         if (line.contains("comando:")) {
             line = line.substring(8);
         }
             comando+=line;

    }
    text.setText(comando.trim());
    }

    public void execConf(String path,JTextArea text) throws FileNotFoundException, IOException{
        String comando = "";
        String Log ="";
            Scanner in = new Scanner(new FileReader(path));
            while (in.hasNextLine()) {
               String line = in.nextLine();
                 if (line.contains("comando:")) {
                     line = line.substring(8);
                   }
                 comando+=line;
            }
            Runtime runtime = Runtime.getRuntime();
                    String cmds[] = {"cmd", "/c", comando};

            Process proc = runtime.exec(cmds);

            InputStream inputstream = proc.getInputStream();

            InputStreamReader inputstreamreader = new InputStreamReader(inputstream,"cp850");
           LineNumberReader lineCounter = new LineNumberReader(inputstreamreader);

            BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
               String line;
            while ((line = bufferedreader.readLine()) != null) {

                      Log += line+"\n";
                      text.setText("");
                      text.setText(Log);
                      text.update(text.getGraphics());

                   }

    }

    public void escritor(String path,String comando ){
                   Path caminho = Paths.get(path);
                   byte[] bytes = comando.getBytes();
                   try {
                    Files.write(caminho, bytes);
                } catch (Exception e) {
                }
                  JOptionPane.showMessageDialog(null, "Os comandos foram salvos!");
            }

    public  void pararTudo(JTextArea logger){
       String comando[] ={"cmd", "/c","ping -n 10 \\192.168.0.10"
              + "& echo feito"};
         logger.setText("");
     /*String comando[] = {"cmd ", "/c","sc \\\\192.168.0.18 stop TOTVS-Appserver12-COLABORACAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-CAIXAS & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-MASTER_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE1_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE2_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE3_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE4_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE5_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE6_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE7_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE8_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE9_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE10_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE11_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE12_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE14_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE15_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE16_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE17_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE18_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE19_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE20_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE21_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE22_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE23_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE24_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE25_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE26_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE27_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE28_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE29_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-SLAVE30_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TOTVS-Appserver12-TELNET_PRODUCAO & " +
                    "sc \\\\192.168.0.18 stop TSS-Appserver12_PRODUCAO & " +
                    "sc \\\\192.168.0.22 stop DBACCESS & " +
                    "sc \\\\192.168.0.18 stop licenseVirtual"};*/

           String ErroLog ="";
             Runtime runtime = Runtime.getRuntime();


            try {
            Process proc2 = runtime.exec(comando);
                    InputStream inputstream = proc2.getInputStream();
                    InputStreamReader inputstreamreader = new InputStreamReader(inputstream,"cp850");
                    LineNumberReader lineCounter = new LineNumberReader(inputstreamreader);
                    BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
                       String line;
                     while ((line = bufferedreader.readLine()) != null) {
                      ErroLog += line+"\n";
                      logger.setText("");
                      logger.setText("Parando tudo...");
                      logger.setText(ErroLog);
                      logger.update(logger.getGraphics());
                   }
        } catch (Exception e) {
        }
    }

    public  void TrocarRPO(JTextArea logger){
     logger.setText("AAAAAAAAA");
 String comando[] = {"cmd ", "/c","@echo off & " +
                        "cls & " +
                        "echo Processo de copia e Atualização de RPO & " +
                        "echo %date% & " +
                        "echo Passo 1 - Criando Backup & " +
                        "echo Backup em processamento... & " +
                        "echo Copia dos RPO: Em andamento. & " +
                        "copy C:\\TOTVS12_PRODUCAO\\Microsiga\\Protheus\\apo\\tttp120.rpo C:\\TOTVS12_PRODUCAO\\Microsiga\\Protheus\\apoBackup & " +
                        "cd C:\\TOTVS12_PRODUCAO\\Microsiga\\Protheus\\apoBackup & " +
                        "ren tttp120.rpo %date:~6,4%%date:~3,2%%date:~0,2%.rpo  & " +
                        "echo Backup do RPO: Concluida. & " +
                        "echo Processo de copia e Atualizacao de RPO & " +
                        "echo %date% & " +
                        "echo Passo 2 - Atualizar RPO em producao & " +
                        "echo Atualizando RPO... & " +
                        "echo Atualizando RPO. & " +
                        "copy C:\\TOTVS12_PRODUCAO\\Microsiga\\Protheus\\apo1260\\tttp120.rpo C:\\TOTVS12_PRODUCAO\\Microsiga\\Protheus\\apo & " +
                        "echo Atualizacao concluida..."};

       String ErroLog ="";
         Runtime runtime = Runtime.getRuntime();

        
        try {
        Process proc2 = runtime.exec(comando);
               	InputStream inputstream = proc2.getInputStream();
                InputStreamReader inputstreamreader = new InputStreamReader(inputstream,"cp850");
                LineNumberReader lineCounter = new LineNumberReader(inputstreamreader);
                BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
                   String line;
                 while ((line = bufferedreader.readLine()) != null) {
                  
                     ErroLog += line+"\n\n";
                  logger.setText("");
                  logger.setText(ErroLog);
                  logger.update(logger.getGraphics());
                  
               }
    } catch (Exception e) {
    }
}

}

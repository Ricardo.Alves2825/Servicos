/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author suporte
 */
public class PINTAR_TABELA extends DefaultTableCellRenderer{
public PINTAR_TABELA(){}

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel label =(JLabel)    super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //To change body of generated methods, choose Tools | Templates.
     
        //Color background = Color.WHITE;
        Object objeto = table.getValueAt(row, 1);
        //label.setBackground(background);
           table.setSelectionBackground(Color.ORANGE);
                table.setSelectionForeground(Color.CYAN);
                label.setFont(new Font("Tahoma",Font.BOLD,12));
        try {
            String status = objeto.toString();
            if (status.equalsIgnoreCase("running")) {
                
                label.setForeground(new Color(0,200,0));
                
            }
            else{label.setForeground(new Color(255,0,0));}
        } catch (Exception e) {
        }

        
        return label;
    }
    
}
